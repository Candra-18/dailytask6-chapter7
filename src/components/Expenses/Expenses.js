import React, { useState } from "react";

// import ExpenseItem from "./ExpenseItem";
import ExpensesFilter from "./ExpensesFilter";
import Card from "../UI/Card";
import "./Expenses.css";
import ExpensesList from "./ExpensesList";
import ExpensesChart from './ExpensesChart';

const Expenses = (props) => {
  const [filteredYear, setFilteredYear] = useState('all');

  const filterChangeHandler = selectedYear => {
    setFilteredYear(selectedYear);
  }
  
  var filteredExpenses = props.items;
  if (filteredYear !== 'all') {
    filteredExpenses = props.items.filter(item => {
      return (
        item.date.getFullYear() === parseInt(filteredYear)
      )
    });
  }


  // const filteredExpenses = props.items.filter(expense => {
  //   return expense.date.getFullYear().toString() === filteredYear;
  // })

  // let expenseContent = <p>No Expense Found</p>
  // if (filteredExpenses.length > 0) {
  //   expenseContent = filteredExpenses.map((item) => {
  //     return <ExpenseItem
  //       key={item.id}
  //       title={item.title}
  //       amount={item.amount}
  //       date={item.date}
  //     />
  //   })
  // }

  return (
    <Card className="expenses">
      <ExpensesFilter selected={filteredYear} onChangeFilter={filterChangeHandler} />
      <ExpensesChart expenses={filteredExpenses} />
      <ExpensesList items={filteredExpenses}/>
      {/* {expenseContent} */}
      
      
      {/* {filteredExpenses.length === 0 && <p>No expenses to show</p>}
      {filteredExpenses.length > 0 && filteredExpenses.map((item) => {
        return (
          <ExpenseItem
            key={item.id}
            title={item.title}
            amount={item.amount}
            date={item.date}
          />
        )
      })} */}


      {/* {filteredExpenses.length === 0 ? 
      (<p>No Expense Found</p>) : (
        filteredExpenses.map((item) => {
          return (
            <ExpenseItem 
              key={item.id}
              title={item.title}
              amount={item.amount}
              date={item.date}
            />
          )
        })
      ) } */}

    </Card>
  );
}

export default Expenses;