import React, {useState} from "react";
import ExpensesForm from "./ExpensesForm";

import "./NewExpense.css";

const NewExpenses = (props) => {

  const [isAddNewData,setIsAddNewData] = useState(false)

    const saveExpenseDataHandler = (enteredExpenseData) =>{
        const expenseData = {
            ...enteredExpenseData,
            id: Math.random().toString()
        }
        console.log("di new expense")
        console.log(expenseData)
        props.onAddExpense(expenseData)
        
    }

    const startAddNewData = ()=>{
      setIsAddNewData(true)
    }
    const cancelAddNewData = ()=>{
      setIsAddNewData(false)
    }
    return (
      <div className='new-expense'>
        {!isAddNewData && <button onClick={startAddNewData}>Add New Expense</button>}
        {isAddNewData && <ExpensesForm onSaveExpenseData={saveExpenseDataHandler} onCancel={cancelAddNewData} />}
      </div>
    )
  
}

export default NewExpenses;
